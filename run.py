import matplotlib.pyplot as plt
from classifier.config import logger
from classifier.models.base import SKLearnModel
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from classifier.helpers.util import get_data_from_files


def main():
    # Get training and testing data from files
    logger.info("Loading data from CSV files")
    train_data, test_data = get_data_from_files("classifier/data/train.csv", "classifier/data/test.csv")

    accuracies = {}

    sklearn_models = [
        (DecisionTreeClassifier(), "Tree"),
        (RandomForestClassifier(), "Forest"),
        (MLPClassifier(), "NeuralNet")
    ]

    for tup in sklearn_models:
        model_type = tup[0]
        name = tup[1]
        model = SKLearnModel(train_data, test_data, model_type)
        accuracy = model.run()
        accuracies[name] = accuracy

    # Plot accuracy values
    x_axis = [key for key in accuracies]
    y_axis = [accuracies[key] for key in accuracies]
    plt.bar(x=x_axis, height=y_axis)
    plt.ylim((0.5,1))
    # plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()
