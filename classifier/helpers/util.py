import pandas as pd


def get_data_from_files(train_filepath, test_filepath):
    """
    Gets dataframe from train and test csv files
    :param train_filepath: string: path to train.csv
    :param test_filepath: string: path to test.csv
    :return: pandas dataframe with train data, pandas dataframe with test data
    """
    train_data = pd.read_csv(train_filepath)
    test_data = pd.read_csv(test_filepath)
    return train_data, test_data
