import os
import yaml
import logging.config


def setup_logger():
    # get the environment to use for logging
    env = os.environ.get("ENVIRONMENT", "local")
    # set the logging config for entire application using correct yaml file based on environment
    return logging.config.dictConfig(read_config(f"configurations/{env}.logging.yaml"))


def read_config(filename):
    """
    Helper method for converting yaml config files to dictionary. Filename must be a '.yaml' file.
    :param filename: str: Filename to read/convert.  Must be a '.yaml' file
    :return: dict: dictionary containing configuration options from <filename>
    """
    # for cleanliness get our directory and mark as an internal var _
    _dir = os.path.dirname(os.path.realpath(__file__))
    # get the full path to the config file
    filepath = os.path.join(_dir, f"{filename}")
    # read the yaml file into a dictionary and return the dict
    with open(filepath, "rb") as config:
        return yaml.safe_load(config)


# Logging config
setup_logger()
logger = logging.getLogger(__name__)
