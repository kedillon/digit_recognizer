import logging

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split


class SKLearnModel:
    @property
    def log(self):
        """
        Called as self.log.[info,debug,warn,error]("important message")
        e.g. self.log.info("an information logger!")
        :return: logger()
        """
        name = '.'.join([self.__module__, self.__class__.__name__])
        return logging.getLogger(name)

    def __init__(self, train_df, test_df, model):
        """
        Initializes the model
        :param train_df: training dataframe
        :param test_df: testing dataframe
        :param model: the sklearn model to use
        """
        self.log.info(f"Initializing model: {self.__class__.__name__}")
        self.train = train_df
        self.test = test_df
        self.model = model

    def _split(self):
        """
        Separates labels and parameters and performs a train_test_split
        :return: X_train, X_test, y_train, y_test
        """
        # Gets parameters and labels
        X = self.train.iloc[:, 1:].values
        y = self.train.iloc[:, 0].values

        return train_test_split(X, y, test_size=0.33)

    def _train_model(self, x_train, y_train):
        self.log.info(f"Training Neural Network on {len(y_train)} samples")
        self.model = self.model.fit(x_train, y_train)

    def _predict(self, x_test):
        """
        Makes predictions using self.model
        :param x_test: numpy array [[],[],[]]: list of lists of passenger params
        :return: accuracy score, list of predictions
        """
        self.log.info(f"Predicting digits for {len(x_test)} samples")
        # Predict for X_test data
        y_pred = self.model.predict(x_test)
        return y_pred

    def _compute_accuracy(self, y_test, y_pred):
        self.log.info(f"Computing accuracy")
        # Compare to y_test and calculate accuracy
        accuracy = accuracy_score(y_test, y_pred)
        return accuracy

    def run(self):
        # Parse and split data into training sets
        X_train, X_test, y_train, y_test = self._split()
        # Train model
        self._train_model(X_train, y_train)
        # Make predictions and calculate accuracy
        y_pred = self._predict(X_test)
        return self._compute_accuracy(y_test, y_pred)

